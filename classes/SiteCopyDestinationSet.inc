<?php

class SiteCopyDestinationSet {

  private $objects = array();

  public function __construct() {

  }

  public function getDestinationHandlers() {
    return array_keys($this->objects);
  }

  public function getDestinationObjects($key) {
    if (!isset($this->objects[$key])) {
      return array();
    }
    return $this->objects[$key];
  }

  public function addDestinationObject($className, $fields, $args = array()) {
    if (!isset($this->objects[$className])) {
      $this->objects[$className] = array();
    }
    $this->objects[$className][] = array(
      'class' => $className,
      'fields' => $fields,
      'args' => $args,
    );
  }

}
