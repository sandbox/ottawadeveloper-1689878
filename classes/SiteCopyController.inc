<?php

class SiteCopyController {

  const MODE_IMPORT = 1;
  const MODE_REVERT = 2;

  private $importTasks = array();

  private $maxTime = 45;
  private $maxSteps = 100;

  private $currentTask = 0;

  private $mode = self::MODE_IMPORT;

  public function __construct(array $state = NULL) {
    if (empty($state)) {
      $state = array();
    }
    $state += array(
      'mode' => self::MODE_IMPORT,
      'max_time' => 45,
      'max_steps' => 100,
    );
    if (!isset($state['current_task'])) {
      if ($state['mode'] == self::MODE_IMPORT) {
        $state['current_task'] = 0;
      } else {
        $state['current_task'] = -1;
      }
    }
    $this->currentTask = filter_var($state['current_task'],
        FILTER_SANITIZE_NUMBER_INT);
    $this->maxTime = filter_var($state['max_time'],
        FILTER_SANITIZE_NUMBER_INT);
    $this->maxSteps = filter_var($state['max_steps'],
        FILTER_SANITIZE_NUMBER_INT);
    $this->setMode($state['mode']);
  }

  public function getState() {
    return array(
      'mode' => $this->mode,
      'max_time' => $this->maxTime,
      'max_steps' => $this->maxSteps,
      'current_task' => $this->currentTask,
    );
  }

  private function setMode($mode) {
    if (!in_array($mode, array(self::MODE_IMPORT, self::MODE_REVERT))) {
      $this->mode = self::MODE_IMPORT;
    } else {
      $this->mode = $mode;
    }
  }

  public function addImportTask(SiteCopyImportTask $task, $position = -1) {
    if ($position < 0) {
      $this->importTasks[] = $task;
    } elseif ($position == 0) {
      array_unshift($this->importTasks, $task);
    } elseif ($position >= count($this->importTasks)) {
      $this->importTasks[] = $task;
    } else {
      for ($k = count($importTasks) - 1; $k >= $position; $k--) {
        $this->importTasks[$k + 1] = $this->importTasks[$k];
      }
      $this->importTasks[$position] = $task;
    }
  }

  public function execute() {
    echo 'beginning execution';
    $steps = 0;
    $time = time();
    $max_tasks = count($this->importTasks);
    if ($this->currentTask == -1) {
      $this->currentTask = $max_tasks - 1;
    }
    $continue = TRUE;
    $method = 'executeStep';
    $step = 1;
    if ($this->mode == self::MODE_REVERT) {
      $method = 'revertStep';
      $step = -1;
    }
    while ($continue) {
      $steps++;

      if (!$this->importTasks[$this->currentTask]->$method()) {
        $this->currentTask += $step;
      }
      $continue = $continue
          && ($this->currentTask >= 0) // task must be positive still
          && ($this->currentTask < $max_tasks) // task must exist
          && ($steps < $this->maxSteps) // cap on number of steps
          && ((time() - $time) < $this->maxTime); // cap on import time
    }
  }

  public function getErrors() {
    $errors = array();
    foreach ($this->importTasks as $task) {
      $errors += $task->getErrors();
    }
    return $errors;
  }

  public function getErrorCount() {
    $count = 0;
    foreach ($this->importTasks as $task) {
      $count += $task->getErrorCount();
    }
    return $count;
  }

  public function getProcessedCount() {
    $count = 0;
    foreach ($this->importTasks as $task) {
      $count += $task->getProcessedCount();
    }
    return $count;
  }

  public function getCompletedCount() {
    $count = 0;
    foreach ($this->importTasks as $task) {
      $count += $task->getCompletedCount();
    }
    return $count;
  }

  public function getTotalCount() {
    $count = 0;
    foreach ($this->importTasks as $task) {
      $count += $task->getTotalCount();
    }
    return $count;
  }
}
