<?php

abstract class SiteCopyImportTask {

  private $errors = array();
  private $errorCount = 0;
  private $processedCount = 0;
  private $completedCount = 0;

  public function getErrors() {
    return $this->errors;
  }

  public function getErrorCount() {
    return $this->errorCount;
  }

  public function getProcessedCount() {
    return $this->processedCount;
  }

  public function getCompletedCount() {
    return $this->completedCount;
  }

  public function executeStep() {
    $txn = db_transaction('import-step-execute');
    try {
      $this->processedCount++;
      if ($this->execute() === FALSE) {
        $this->processedCount--;
        return FALSE;
      }
      $this->completedCount++;
    } catch (Exception $e) {
      $this->errorCount++;
      $this->errors[] = $e->getMessage();
      $txn->rollback();
      return FALSE;
    }
    return TRUE;
  }

  public function revertStep() {
    $txn = db_transaction('import-step-revert');
    try {
      $this->processedCount++;
      if ($this->revert() === FALSE) {
        $this->processedCount--;
        return FALSE;
      }
      $this->completedCount++;
    } catch (Exception $e) {
      $this->errorCount++;
      $this->errors[] = $e->getMessage();
      $txn->rollback();
      return FALSE;
    }
    return TRUE;
  }

  public abstract function execute();
  public abstract function revert();
  public abstract function getTotalCount();

}
