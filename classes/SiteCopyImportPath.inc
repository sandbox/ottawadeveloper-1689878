<?php

abstract class SiteCopyImportPath {

  public abstract function __construct(array $args);

  public abstract function getDestinationObjects(&$source_item);

}
