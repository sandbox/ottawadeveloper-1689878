<?php

class SiteCopyInventory {

  private $cache = array();
  private $donePrimary = FALSE;
  private $cachedState = '';

  public function getNextItem($state = 'R') {
    if (empty($this->cache) ||
        (!count($this->cache)) ||
        ($this->cachedState != $state)) {
      if (!$this->loadIntoCache($state)) {
        return FALSE;
      }
    }
    $nextID = array_shift($this->cache);
    return sitecopy_page_load($nextID);
  }

  public function getTotal() {
    $rs = db_query('SELECT * FROM {sitecopy_source_item}');
    return $rs->rowCount();
  }

  public function getDestinationKeys($source_item) {
    $keys = array();
    $rs = db_query('
      SELECT *
      FROM {sitecopy_destination_item}
      WHERE source_id = :iid
    ', array(
      ':iid' => $source_item->iid
    ));
    foreach ($rs as $row) {
      if (!isset($keys[$row->destination_class])) {
        $keys[$row->destination_class] = array();
      }
      $keys[$row->destination_class][] = (array) $row;
    }
    return $keys;
  }

  private function loadIntoCache($state = 'R') {
    $this->cachedState = $state;
    $this->cache = array();
    $rs = NULL;
    if (!$this->donePrimary) {
      $rs = db_query("
        SELECT iid
        FROM {sitecopy_source_item}
        WHERE
          iid = tiid
          AND state = :state
        LIMIT 50
      ", array(
        ':state' => $state,
      ));
      if (!$rs->rowCount()) {
        $this->donePrimary = TRUE;
        return $this->loadIntoCache();
      }
    } else {
      $rs = db_query("
        SELECT iid
        FROM {sitecopy_source_item}
        WHERE state = :state
        LIMIT 50
      ", array(
        ':state' => $state,
      ));
    }
    foreach ($rs as $row) {
      $this->cache[] = $row->iid;
    }
    return !!count($this->cache);
  }

  public function tagItemImported($source_item, $destination_objects) {
    foreach ($destination_objects as $obj) {
      db_insert('sitecopy_destination_item')
      ->fields(array(
        'source_id' => $source_item->iid,
        'destination_class' => $obj['classname'],
        'destination_key' => serialize($obj['key']),
      ))
      ->execute();
    }
    db_update('sitecopy_source_item')
    ->fields(array(
      'state' => 'I',
    ))
    ->condition('iid', $source_item->iid)
    ->execute();
  }

  public function resetSourceItem($source_item) {
    db_delete('sitecopy_destination_item')
    ->condition('source_id', $source_item->iid)
    ->execute();
    db_update('sitecopy_source_item')
    ->fields(array(
      'state' => 'R',
    ))
    ->condition('iid', $source_item->iid)
    ->execute();
  }

}
