<?php

class SiteCopyPageImportTask extends SiteCopyImportTask {

  private $inventory = NULL;

  public function __construct() {
    $this->inventory = new SiteCopyInventory();
  }

  public function execute() {
    echo '<br />beginning page import execute';
    $source_item = $this->inventory->getNextItem();
    echo '<br />';
    var_dump($source_item);
    if (empty($source_item)) {
      return FALSE;
    }
    $destination_objects = $this->processSourceItem($source_item);
    echo '<br />';
    var_dump($destination_objects);
    $destination_keys = $this->processDestinationItems($destination_objects);
    $this->inventory->tagItemImported($source_item, $destination_keys);
    return TRUE;
  }

  private function processDestinationItems(SiteCopyDestinationSet $dobjs) {
    $keys = array();
    foreach ($dobjs->getDestinationHandlers() as $handler_name) {
      echo '<hr />';
      echo $handler_name;
      $handler = new $handler_name();
      foreach ($dobjs->getDestinationObjects($handler_name) as $obj) {
        echo '<br />';
        var_dump($obj);
        $keys[] = array(
          'key' => $handler->saveDestinationObject($obj),
          'classname' => $handler_name,
        );
      }
    }
    return $keys;
  }

  private function processSourceItem($source_item) {
    $path_name = 'SiteCopyBasicImportPath';
    $args = array();
    if (!empty($source_item->path_name)) {
      $path_name = $source_item->path_name;
      if (!empty($source_item->path_args)) {
        $args = unserialize($source_item->path_args);
      }
    }
    $path_obj = new $path_name($args);
    return $path_obj->getDestinationObjects($source_item);
  }

  public function revert() {
    $source_item = $this->inventory->getNextItem('I');
    if (empty($source_item)) {
      return FALSE;
    }
    $destination_keys = $this->inventory->getDestinationKeys($source_item);
    foreach ($destination_keys as $class => $objects) {
      $handler = new $class();
      foreach ($objects as $obj) {
        $handler->revertDestinationObject($obj);
      }
    }
    $this->inventory->resetSourceItem($source_item);
    return TRUE;
  }

  public function getTotalCount() {
    return $this->inventory->getTotal();
  }

}
