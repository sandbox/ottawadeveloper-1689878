<?php

class SiteCopyNodeDestination extends SiteCopyDestination {

  public function saveDestinationObject($object) {
    $node = (object) $object['fields'];
    node_object_prepare($node);
    $node->revision = 1;
    node_save($node);
    return array(
      'nid' => $node->nid,
      'vid' => $node->vid,
      'old_vid' => !empty($node->old_vid) ? $node->old_vid : NULL,
    );
  }

  public function revertDestinationObject($dkey) {
    if (!empty($dkey['old_vid'])) {
      $node = node_load($dkey['nid'], $dkey['old_vid']);
      $node->revision = 1;
      unset($node->vid);
      node_save($node);
    } else {
      node_delete($dkey['nid']);
    }
  }

}
