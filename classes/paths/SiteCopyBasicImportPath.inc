<?php

class SiteCopyBasicImportPath extends SiteCopyImportPath {

  public function __construct(array $args) {

  }

  public function getDestinationObjects(&$source_item) {
    $set = new SiteCopyDestinationSet();
    $set->addDestinationObject('SiteCopyNodeDestination', array(
      'body' => '<p>Hello World!</p>',
      'type' => 'article',
    ));
    return $set;
  }

}
