<?php

abstract class SiteCopyDestination {

  public abstract function saveDestinationObject($object);

  public abstract function revertDestinationObject($object);
}
