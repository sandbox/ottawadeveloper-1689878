<?php

function sitecopy_content_inventory_home() {
  return '<p>Placeholder: sitecopy_content_inventory_home</p>';
}

function sitecopy_page_edit_form_validate($form, &$form_state) {
  $vals = (object) $form_state['values'];
  field_attach_form_validate('sitecopy_page', $vals, $form, $form_state);
}

function sitecopy_page_edit_form_submit($form, &$form_state) {
  $vals = (object) $form_state['values'];
  field_attach_submit('sitecopy_page', $vals, $form, $form_state);
  $obj = sitecopy_page_save($vals);
  $form_state['redirect'] = sitecopy_page_uri($obj->iid, TRUE);
  drupal_set_message(t('Source page saved'));
}

function sitecopy_page_edit_form($form, &$form_state, $sitecopy_page = NULL) {
  if (empty($sitecopy_page)) {
    $sitecopy_page = (object) array(
      'iid' => NULL,
      'uuid' => NULL,
      'language' => 'en',
      'tiid' => 0,
    );
  }
  $form['iid'] = array(
    '#type' => 'value',
    '#value' => $sitecopy_page->iid,
  );
  $form['language'] = array(
    '#type' => 'value',
    '#value' => $sitecopy_page->language,
  );
  field_attach_form('sitecopy_page', $sitecopy_page, $form, $form_state);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function sitecopy_page_delete_form_submit($form, &$form_state) {
  sitecopy_page_delete($form_state['values']['sitecopy_page']);
  drupal_set_message(t('Source page deleted'));
  $form_state['redirect'] = 'admin/content/sitecopy';
}

function sitecopy_page_delete_form($form, &$form_state, $sitecopy_page) {
  $form['sitecopy_page'] = array(
    '#type' => 'value',
    '#value' => $sitecopy_page,
  );
  return confirm_form(
      $form,
      t('Are you sure you want to delete this source page?'),
      sitecopy_page_uri($sitecopy_page, TRUE),
      NULL,
      t('Yes')
  );
}
