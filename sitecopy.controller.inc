<?php

class SiteCopyPageController extends DrupalDefaultEntityController {

  public function delete($sitecopy_page) {
    $query = db_delete('sitecopy_source_item');
    if (!empty($sitecopy_page->iid)) {
      $query->condition('iid', $sitecopy_page->iid);
    } else if (!empty($sitecopy_page->uuid)) {
      $query->condition('uuid', $sitecopy_page->uuid);
    } else {
      return FALSE;
    }
    $query->execute();
    field_attach_delete('sitecopy_page', $sitecopy_page);
    module_invoke_all('entity_delete', $sitecopy_page, 'sitecopy_page');
    return TRUE;
  }

  public function save($sitecopy_page) {
    $primary_key = array();
    if (!empty($sitecopy_page->iid)) {
      $primary_key[] = 'iid';
    } elseif (!empty($sitecopy_page->uuid)) {
      $primary_key[] = 'uuid';
    } else {
      $sitecopy_page->uuid = uniqid('drupal://sitecopy/sitecopy_page/', TRUE);
    }
    $result = drupal_write_record('sitecopy_source_item', $sitecopy_page, $primary_key);
    if ($result == SAVED_NEW) {
      field_attach_insert('sitecopy_page', $sitecopy_page);
      module_invoke_all('entity_insert', $sitecopy_page, 'sitecopy_page');
    } else {
      field_attach_update('sitecopy_page', $sitecopy_page);
      module_invoke_all('entity_update', $sitecopy_page, 'sitecopy_page');
    }
    return $sitecopy_page;
  }

}