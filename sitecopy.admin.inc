<?php

function sitecopy_pages_admin() {
  return '<p>Placeholder: sitecopy_pages_admin</p>';
}

function sitecopy_import_start() {
  $controller = new SiteCopyController();
  $controller->addImportTask(new SiteCopyPageImportTask());
  $controller->execute();
  echo '<hr />';
  echo '<p>';
  echo $controller->getProcessedCount() . ' items; ';
  echo $controller->getCompletedCount() . ' completed, ';
  echo $controller->getErrorCount() . ' errored.';
  echo '</p>';
  $errors = $controller->getErrors();
  if (count($errors)) {
    echo implode('<br />', $errors);
  }
}